
# PlanAhead Launch Script for Pre-Synthesis Floorplanning, created by Project Navigator

create_project -name My_UART -dir "E:/Creator/Creator_H/Xilinx_Proj/My_UART/planAhead_run_2" -part xc6slx9tqg144-2
set_param project.pinAheadLayout yes
set srcset [get_property srcset [current_run -impl]]
set_property target_constrs_file "My_UART.ucf" [current_fileset -constrset]
set hdlfile [add_files [list {My_UART.vhd}]]
set_property file_type VHDL $hdlfile
set_property library work $hdlfile
set_property top My_UART $srcset
add_files [list {My_UART.ucf}] -fileset [get_property constrset [current_run]]
open_rtl_design -part xc6slx9tqg144-2
