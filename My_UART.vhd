----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    15:25:10 08/08/2014 
-- Design Name: 
-- Module Name:    My_UART - Behavioral 
-- Project Name: 
-- Target Devices:
-- Tool versions: 
-- Description: 
--
-- Dependencies:
-- 
-- Revision: 
-- Revision 0.01 - File Created   
-- Additional Comments:  
--
----------------------------------------------------------------------------------
library IEEE; 
use IEEE.STD_LOGIC_1164.ALL;  
use IEEE.std_logic_unsigned.all;
use IEEE.NUMERIC_STD.ALL; 
library UNISIM;
use UNISIM.VComponents.all;   
------------------------------------------------------------------------------------------
entity My_UART is
	port (
			clk		:  in		std_logic;							-- ext. 25 MHz osc.
			rx			:	in		std_logic;							-- UART receiver input
			rx_byte	:	out	std_logic_vector (7 downto 0) -- received byte
		  );
end My_UART;
------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------
architecture Behavioral of My_UART is

constant		clk_freq		:	integer := 25_000_000; 						-- Osc. frequency 
constant		uart_baud	:	integer := 115200;							-- UART baudrate
constant 	uart_freq 	: 	integer := clk_freq / (32 * uart_baud);-- UART system clock

signal		uart_divider:	integer range 0 to uart_freq;				-- UART divider
signal		uart_clk		:	std_logic;										-- UART divider
signal 		cnt 			: 	integer range 0 to 15;							-- UART clk counter
signal		sample		:	std_logic;										-- Flag of begining 

begin

process (clk)											-- sys_clk divider for generating uart_clk
begin
	if (clk = '1' and clk'event) then
		uart_divider <= uart_divider + 1;
		if (uart_divider = uart_freq) then
			uart_clk <= uart_clk xor '1';
			uart_divider <= 0;
		end if;
	end if;
end process;

process (uart_clk)
	
variable		bitn	:	integer range 0 to 9 := 0;			-- number of received bit
variable		data	:	std_logic_vector (7 downto 0);	-- received byte
variable		rx_s	:	std_logic_vector (2 downto 0);	-- bit sample variable
variable		rx_b	:	std_logic;								-- received bit true value

begin
	if (uart_clk = '1' and uart_clk'event) then
		if (rx_b = '1' and rx = '0') then					-- start condition checking
			sample <= '1';
			rx_b := rx;
		else
			rx_b := rx;
		end if;

	if (cnt = 8) then
		rx_s(0) := rx;
	end if;

	if (cnt = 9) then
		rx_s(1) := rx;
	end if;

	if (cnt = 10) then
		rx_s(2) := rx;
	end if;
	
	if (cnt = 11) then
		rx_b := ((rx_s(0) and rx_s(1)) or (rx_s(1) and rx_s(2)) or ((rx_s(0) and rx_s(2))));
		case (bitn) is
			when 0 => 	if (rx = '0') then
								bitn := 1;
							else
								bitn := 0;
								sample <= '0';
							end if;
							
			when 1 =>	data(0) := rx_b;
							bitn := 2;
							
			when 2 =>	data(1) := rx_b;
							bitn := 3;
							
			when 3 =>	data(2) := rx_b;
							bitn := 4;
							
			when 4 =>	data(3) := rx_b;
							bitn := 5;
							
			when 5 =>	data(4) := rx_b;
							bitn := 6;
							
			when 6 =>	data(5) := rx_b;
							bitn := 7;				
							
			when 7 =>	data(6) := rx_b;
							bitn := 8;		
							
			when 8 =>	data(7) := rx_b;
							bitn := 9;		
							
			when 9 =>	bitn := 0;
							sample <= '0';
							if (rx = '1') then	
								rx_byte <= data;
							end if;
		end case;
	end if;
	end if;
end process;

process (uart_clk)		 -- sys_clk divider for generating uart_clk
begin
	if (uart_clk = '1' and uart_clk'event) then       
       if (sample = '1') then
			cnt <= cnt + 1;
       else
			cnt <= 0;
       end if;
	end if;
end process;

end Behavioral;











